# README #

This provides awscli and some minifying tools. 

It is adapted from the great folks at Bad Sector Labs here: 
https://gitlab.com/badsectorlabs/aws-compress-and-deploy/-/blob/master/Dockerfile

### Usage ###

You can find the build for this docker at:

https://hub.docker.com/repository/docker/nimnaij/node-aws

### I have a problem.  ###

For issues with this repo hit me up on twitter @nimnaij
