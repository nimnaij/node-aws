FROM node:latest

RUN npm install -g browserify && \
    npm install -g uglify-js && \
    npm install -g html-minify && \
    npm install -g uglifycss


WORKDIR /tmp/

RUN apt-get update && apt-get install unzip curl -y

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" > "/tmp/awscliv2.zip" && \ 
    unzip /tmp/awscliv2.zip && mkdir -p /opt/awscli/{aws,bin} && ./aws/install -i "/opt/awscli/aws" -b "/opt/awscli/bin"; rm /tmp/awscliv2.zip 

ENV PATH="/opt/awscli/bin:${PATH}"

